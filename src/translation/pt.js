module.exports = {
	title: 'Espectro de Gênero e Sexualidade',
	description: 'Onde você está no Espectro de Gênero e Sexualidade?',
	keywords: 'gênero, identidade, expressão, sexualidade, orientação, impulso, romântico, desejo, relacionamento, homem, não-binário, mulher, masculino, feminino, heterossexual, bi, pan, gay, assexual, ace, arromântico, romântico, monogamia, poliamor, baunilha, bdsm',
	generate: 'Compartilhe o seu',
	generateHelper: '(clique nos sliders para selecionar valores)',
	mine: 'Meu Espectro de Gênero e Sexualidade',
	author: {
			name: 'Andrea Vos',
			email: 'andrea@avris.it',
			attribution: 'Feito com 💜 por Andrea Vos',
			support: 'Me pague uma cerveja',
	},
	translation: {
		attribution: 'Traduzido por',
		name: 'Diogo de Souza',
		link: 'https://gitlab.com/sozua',
	},
	source: 'Código fonte',
	axes: {
			genderIdentity: {
					label: 'Identidade de Gênero',
					left: 'Homem',
					middle: 'Não-binário',
					right: 'Mulher',
			},
			genderExpression: {
					label: 'Expressão de Gênero',
					left: 'Hipermasculino',
					middle: 'Andrógino',
					right: 'Hiperfeminino',
			},
			sexualOrientation: {
					label: 'Orientação Sexual',
					left: 'Heterossexual',
					middle: 'Bi / Pan',
					right: 'Gay',
			},
			sexualDrive: {
					label: 'Desejo Sexual',
					left: 'Nenhum',
					middle: 'Regular',
					right: 'Alto',
			},
			romanticDesire: {
					label: 'Desejo Romântico',
					left: 'Nenhum',
					middle: 'Regular',
					right: 'Alto',
			},
			relationshipAttitude: {
					label: 'Atitude em relação ao relacionamento',
					left: 'Monogâmico',
					middle: 'Aberto',
					right: 'Poliamoroso',
			},
			sexualExploration: {
				label: 'Exploração sexual',
				left: 'Tradicional',
				middle: 'Brincadeiras leves',
				right: 'BDSM pesado',
			},
	},
	share: {
			facebook: 'Compartilhe no Facebook',
			twitter: 'Compartilhe no Twitter',
			text: 'Este é o meu Espectro de Gênero e Sexualidade. Qual o seu?',
			copy: 'Copiar para a área de transferência',
	},
	privacy: {
			header: 'Privacidade',
			noData: 'Todas as informações sobre as opções que você selecionou estão codificados na URL e não estão armazenadas em nenhum local.',
			tracking: 'Eu uso Plausible para analisar o tráfego no website. Todos os dados de visita são anônimos.',
			contact: 'Se você possui alguma dúvida ou deseja solicitar algo relacionados aos seus dados pessoais, sinta-se à vontade para entrar em contato comigo em',
	},
	otherProjects: {
			cake: 'Bolo de Camadas de Atração',
	},
	disclaimer: {
    header: 'Aviso:',
    author: 'Não sou o autor do conceito original desses eixos. ' +
            'Eles estavam circulando online na forma de uma imagem sem marca d\'água ' +
            '– tornando praticamente impossível encontrar o autor. ' +
            'Eu apenas fiz uma versão interativa disso, com alguns ajustes.',
    issues: 'Estou ciente de que esta representação de gênero e sexualidade não é perfeita – mas nenhuma é! ' +
            'Os seres humanos são mais complexos do que apenas alguns eixos!',
    examples: 'Sim, nós, pessoas não-binárias, não estamos necessariamente <em>entre</em> "masculino" e "feminino", ' +
              'sim, agrupar bissexualidade e pansexualidade juntos não é o ideal, etc. etc. etc. ' +
              'Mas é uma aproximação. ' +
              'Se você inventar uma melhor, eu ficarei feliz em fazer um aplicativo para isso 😉',
},

};
